document.getElementById("startCamera").addEventListener("click", startCamera);
document.getElementById("createOffer").addEventListener("click", createOffer);
document.getElementById("setAnswer").addEventListener("click", setAnswer);
document.getElementById("createAnswer").addEventListener("click", createAnswer);


var sdpConstraints = {
  optional: [],
  mandatory: {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
  }
}
var pc1 = null
var streamCamera = null
//var connection = null

function startCamera(){
  initiateConnection()
  //socketBaglan()
  navigator.getUserMedia = navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia
  navigator.getUserMedia({video: true, audio: true}, function (stream) {
    var video = document.getElementById('localVideo')
    video.src = window.URL.createObjectURL(stream)
    video.play()
    streamCamera = stream
   // setupOffer()
  }, function (error) {
    console.log('Error adding stream to pc2: ' + error)
  })

}

//Teklifi Yapan PC

function createOffer(){
  pc1.createOffer(function (desc) {
    pc1.setLocalDescription(desc, function () {}, function () {})
   // connection.send(JSON.stringify(desc, null, 2))
    document.getElementById('localOffer').value = JSON.stringify(desc, null, 2)
  },
  function () { console.warn("Couldn't create offer") },
  sdpConstraints)
}

function setupOffer(){
  var cfg = {'iceServers': [{'url': 'stun:23.21.150.121'}]},
  con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
  pc1 = new RTCPeerConnection(cfg, con)
  pc1.addStream(streamCamera)
  sdc = pc1.createDataChannel("sendDataChannel");


  pc1.onicecandidate = function (evt) {
    //console.log('ICE candidate (pc1)', evt)
    if (evt.candidate == null) {
      var candidate = new RTCIceCandidate({sdpMLineIndex: evt.sdpMLineIndex, sdpMid: evt.sdpMid, candidate: evt.candidate});
      //console.log(candidate);

          // Todo...
          pc1.addIceCandidate(candidate);

     // console.log('ICE candidate (pc1)', JSON.stringify(pc1.localDescription))
   }
 }



 function onsignalingstatechange (state) {
  console.info('signaling state change:', state)
}

function oniceconnectionstatechange (state) {
  console.info('ice connection state change:', state)
}

function onicegatheringstatechange (state) {
  console.info('ice gathering state change:', state)
}

pc1.onsignalingstatechange = onsignalingstatechange
pc1.oniceconnectionstatechange = oniceconnectionstatechange
pc1.onicegatheringstatechange = onicegatheringstatechange

sdc.onopen = function(){

  console.log("CONNECTED!")
};
sdc.onmessage = function(e) {

  console.log("TRACK DATA", e.data);
}
sdc.onaddstream = function(e) {

  console.log("TRACK DATA onaddstream", e.data);
}
pc1.onaddstream = function(e) {

  console.log("TRACK DATA onaddstream", e.data);
}

sdc.ontrack = function(e) {
  console.log("TRACK ontrack", e);
}
sdc.ondatachannel = function(e) {
  console.log("TRACK ontrack", e);
}

}



function onRemoteStreamAdded(event) {
  console.log("Added remote stream", event.stream);
        //remotevid.src = window.URL.createObjectURL(event.stream);
      }

    // when remote removes a stream, remove it from the local video element
    function onRemoteStreamRemoved(event) {
      console.log("Remove remote stream");
        //remotevid.src = "";
      }


      function setOffer(){
        console.log(document.getElementById('setOfferValue').value)
//document.getElementById('setOfferValue').value = JSON.stringify(desc, null, 2)

}

function setAnswer(){

  var CEVAP = document.getElementById('getAnswer').value
  var CEVAPSET = new RTCSessionDescription(JSON.parse(CEVAP));
  pc1.setRemoteDescription(CEVAPSET);
//document.getElementById('setOfferValue').value = JSON.stringify(desc, null, 2)

}

function createAnswer(){
  var pc1Offer = document.getElementById('pc1Offer').value
  var offerDesc = new RTCSessionDescription(JSON.parse(pc1Offer));
  pc1.setRemoteDescription(offerDesc)
  pc1.createAnswer(function (answerDesc) {
    pc1.setLocalDescription(answerDesc)
    document.getElementById('sentAnswer').value = JSON.stringify(answerDesc, null, 2)
  }, function () {console.warn("Couldn't create offer")},
  sdpConstraints);
var count = 0
setInterval(() => {
  count++
  sdc.send('Bak' + count);
  //var streams2 = pc1.getRemoteStreams();
  //for (var streamo of streams2) {
  //  console.log("Remote streams: " + streamo);
  //}
}, 4000)
}

function socketBaglan(){
    // open the websocket
    var wsUrl = "ws://localhost:4200"
    connection = new WebSocket(wsUrl);

    // connection was successful
    connection.onopen = function(event){
      console.log((new Date())+' Connection successfully established');
    };

    // connection couldn't be established
    connection.onerror = function(error){
      console.log((new Date())+' WebSocket connection error: ');
      console.log(error);
    };

    // connection was closed
    connection.onclose = function(event){
      console.log((new Date())+' Connection was closed');
    };

    // this function is called whenever the server sends some data
    connection.onmessage = function(message){
      try {
        var data = JSON.parse(message);
      } catch (e) {
        console.log('valid JSON');
        console.log(message);
        return;
      }

    };
  };
