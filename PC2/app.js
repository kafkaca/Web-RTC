document.getElementById("startCamera").addEventListener("click", startCamera);
document.getElementById("setupOffer").addEventListener("click", setupOffer);
document.getElementById("setOffer").addEventListener("click", setOffer);

var sdpConstraints = {
  optional: [],
  mandatory: {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
  }
}
var pc1 = null
var streamCamera = null
function startCamera(){
  navigator.getUserMedia = navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia
  navigator.getUserMedia({video: true, audio: true}, function (stream) {
    var video = document.getElementById('localVideo')
    video.src = window.URL.createObjectURL(stream)
    video.play()
    streamCamera = stream
  }, function (error) {
    console.log('Error adding stream to pc2: ' + error)
  })

}


function setupOffer(){
  var cfg = {'iceServers': [{'url': 'stun:23.21.150.121'}]},
  con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
  pc1 = new RTCPeerConnection(cfg, con)
  pc1.addStream(streamCamera)
/*
  pc1.createOffer(function (desc) {
    pc1.setLocalDescription(desc, function () {}, function () {})
    document.getElementById('localOffer').value = JSON.stringify(desc, null, 2)
  },
  function () { console.warn("Couldn't create offer") },
  sdpConstraints)

*/
  pc1.onicecandidate = function (e) {
    console.log('ICE candidate (pc1)', e)
    if (e.candidate == null) {
      console.log('ICE candidate (pc1)', JSON.stringify(pc1.localDescription))
    }
  }



  function onsignalingstatechange (state) {
    console.info('signaling state change:', state)
  }

  function oniceconnectionstatechange (state) {
    console.info('ice connection state change:', state)
  }

  function onicegatheringstatechange (state) {
    console.info('ice gathering state change:', state)
  }

  pc1.onsignalingstatechange = onsignalingstatechange
  pc1.oniceconnectionstatechange = oniceconnectionstatechange
  pc1.onicegatheringstatechange = onicegatheringstatechange
}

function setOffer(){
  //console.log(document.getElementById('setOfferValue').value)
//document.getElementById('setOfferValue').value = JSON.stringify(desc, null, 2)
var offer = document.getElementById('setOfferValue').value
pc1.setLocalDescription(offer)
//pc1.setRemoteDescription(new RTCSessionDescription(offer));
pc1.createAnswer(setCreateAnswer, function(){console.log('hata')}, sdpConstraints);
}

function setCreateAnswer(sessionDescription){
   // peerConn.setLocalDescription(sessionDescription);
    console.log("Sending: SDP");
    console.log(sessionDescription);
}
